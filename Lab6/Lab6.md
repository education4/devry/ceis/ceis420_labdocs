           

# Week 6 Lab - Design Patterns

## Scenario

In this week’s lab, you will implement a design pattern.

## Rubric

| Document                         | Points Possible |
| -------------------------------- | --------------- |
| Pattern Listing                  | 20              |
| Implementing the factory pattern | 30              |
| **Total**                        | **50**          |

## List five design patterns commonly used in programming.

1.

2.

3.

4.

5.

## Implementing the factory pattern

 We will create a factory pattern. The factory pattern is useful when you have to create objects based on user input or when you need to defer the instantiation of objects to subclasses. An object is created by calling a factory method. The factory method uses an abstract class for the superclass. Any subclasses extend the super class. Below, you will have an example of a Factory Design Pattern. This is one of the most popular design patterns used in industry today. Use this example to create your own! It can be a factory of colors, shapes, animals, vehicles, toys, food, products, employees, and so on. Anything you want!

Include ALL of your classes (you should have five at the minimum), and a screenshot of the output.

Below is an example using an abstract phone class as the superclass. The two subclasses are iPhone and Android. The PhoneFactory class provides the instantiation of the class. The factory is the main class. Notice that the Samsung and iPhonex variables are of type Phone).

### Phone.java: Abstract superclass

```java
public abstract class Phone {
    //The abstract methods in Phone must be implemented in the subclasses
    public abstract String getRAM();
    public abstract String getStorage();
    public abstract String getSize();
    public abstract void slogan();
	@Override
	public String toString(){
		return
			"RAM= "+this.getRAM()+
			", Storage="+this.getStorage()+
			", Size="+this.getSize();
	}
}
```

### Android.java: subclass 1

```java
public class Android extends Phone{
    private String ram;
	private String storage;
	private String size;
	
	public Android(String r, String s, String sz){
		ram=r;
		storage=s;
		size=sz;
	}
	
	@Override
	public String getRAM() {
		return this.ram;
	}

	@Override
	public String getStorage() {
		return this.storage;
	}

	@Override
	public String getSize() {
		return this.size;
	}
	
	public void slogan()
	{
		System.out.println("Be together, not the same");
	}
}
```

### iPhone.java: subclass 2

```java
public class iPhone extends Phone {
            private String ram;
	private String storage;
	private String size;
	
	public iPhone(String r, String s, String sz){
		ram=r;
		storage=s;
		size=sz;
	}
	@Override
	public String getRAM() {
		return this.ram;
	}

	@Override
	public String getStorage() {
		return this.storage;
	}

	@Override
	public String getSize() {
		return this.size;
	}
        public void slogan()
        {
            System.out.println("Think different");
        }
}
```

### PhoneFactory.java: Factory design pattern

```java
public class PhoneFactory {
    public Phone getPhone(String PhoneType, String r, String s, String sz)
    {
        //Create object here
        if(PhoneType.equalsIgnoreCase("ANDROID"))
            return new Android(r, s, sz); 
        else if(PhoneType.equalsIgnoreCase("IPHONE"))
            return new iPhone(r, s, sz); 
		else
            return null;
    }
}
```

### Factory.java: Main entry

```java
public class Factory {

    public static void main(String[] args) {
		PhoneFactory pf = new PhoneFactory();
        Phone samsung= pf.getPhone("android","6 GB","64 GB","6.0 inches");
		Phone iphonex= pf.getPhone("iphone","3 GB","64 GB","5.8 inches");
		System.out.println("Android Configuration:"+samsung);
        samsung.slogan();
		System.out.println("iPhone Configuration:"+iphonex);
        iphonex.slogan();
    } 
}
```

### Results of the above code:

```
Android Configuration:RAM= 6 GB, Storage=64 GB, Size=6.0 inches

Be together, not the same

iPhone Configuration:RAM= 3 GB, Storage=64 GB, Size=5.8 inches

Think different

BUILD SUCCESSFUL (total time: 0 seconds)
```

### Deliverables

Your code:

Screenshot showing it working: