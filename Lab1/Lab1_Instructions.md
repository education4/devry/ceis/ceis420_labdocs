# Week 1 Lab—Use Eclipse With Java to Call a RESTful Web Service

## Scenario 

In this week’s lab you will call a RESTful web service using Java.  

## Rubric

Point distribution for this activity:

|Document|Points possible|
|:--|:-:|
|Part A|25|	
|Part B|25|	
|Total Points|50|

## PART A:

RESTful web services are an industry standard to communicate with web services. Many web applications you use today use RESTful web and JSON to parse the data. This is prevalent on mobile apps. 
We will be creating a program that uses a RESTful web service call and JSON to parse the returning information. 

1.	Install a Java IDE (either Netbeans, Eclipse). We will be using the RESTful service from [GroupKT](http://www.groupkt.com/services/home.htm) to find the country code for a particular country. There are several RESTful services you can find. Check out government sites or feel free to use: <http://api.geonames.org/postalCodeLookupJSON?postalcode=43207&country=US&username=devry> or <https://afsl.usgovxml.com/welcome.aspx?loc=43065> for some other options. RESTful web services are a common way to make calls out to other systems (Facebook, Twitter, etc.). 
2.	Go to `File -> New Project` and create a new project. If you are using eclipse, create a new class, if using Netbeans your class will be created for you. Enter the following code in your Main. This code will ask the user for a country code. Try a few: IN, US, MM, and so on and see what you get. The output will be in a format we will discuss next. When entering this code, you will need to import several java libraries. If you get an error in your code, click on it and you will get a message to import.

```java
public static void main(String[] args){

	URL serviceRequest = getServiceRequest();
	InputStream rawContent = getContent(serviceRequest);

	printContent(rawContent);

}

public static URL getServiceRequest(){
	URL serviceRequest = null;

	try(Scanner userInput = new Scanner(System.in)){
		//Site to use RESTful web service call
		String serviceURL="http://groupkt.com/country/get/iso2code/"; 
		String countryCode="";

		//Ask the user for the country code
		System.out.println("Please enter the country code: ");
		countryCode = userInput.nextLine().toUpperCase();

		//concatenate string for the RESTful web service
		serviceRequest = new URL(serviceURL+countryCode);

	} catch(Exception e){
		e.printStackTrace();
	}

	return serviceRequest;
}

private static InputStream getContent(URL serviceRequest){

	InputStream rawContent = null;

	try {
		HttpURLConnection connection = 
			(HttpURLConnection) serviceRequest.openConnection();
		serviceRequest.openConnection();
		connection.setRequestMethod("GET");
		rawContent = (InputStream) connection.getInputStream();  
	} catch (Exception e) {
		e.printStackTrace();
	}

	return rawContent;

}

private static void printContent(InputStream rawContent) {
	try{
		InputStreamReader content = new InputStreamReader(rawContent);
		//Read data from input stream
		BufferedReader bufferedContent = new BufferedReader(content);

		String line;
		//Loop and print to console
		while ((line = bufferedContent.readLine()) != null) {
			System.out.println(line);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}         
}
```


3.	When you run the code, you can enter the country code. I typed in IN and got the following response.

```
{
  "RestResponse" : {
    "messages" : [ "Country found matching code [IN]." ],
    "result" : {
      "name" : "India",
      "alpha2_code" : "IN",
      "alpha3_code" : "IND"
    }
  }
}

BUILD SUCCESSFUL (total time: 3 seconds)
```

### Submission 


Copy and paste the result from your code below. Choose a different country from IN and see what you get.


## PART B:

The format this data is in is a common format for web service responses. This is JSON format and is an industry standard. To parse JSON format you need to know the fields, but often when using Web/database you will be pulling data into specific fields. We will use a JAR file to parse this data. Adding JAR files to a Java project is an important skill. Often there will be JAR files that will be needed in your project to improve the functionality. Jar files are zipped files of Java classes. 

1.	Create a new Java Project
2.	Download json.simple http://www.java2s.com/Code/Jar/j/Downloadjsonsimple11jar.htm Unzip it and save it to a location on your computer. 
3.	Next, you need to add it to your classpath. If you are using Netbeans, right click on Libraries and choose Add JAR/Folder. Navigate to the location of your JAR file and pick OPEN. 
 
If you are using Eclipse, right click on your project name and choose `Build path -> Configure Build path`. Click the ClassPath and then choose Add External JARs. Then navigate to the location of your JAR file and choose OPEN.
 
Then add the following code below the `printContent` function definition.

```java
private static void printContentWithJSON(InputStream rawContent){

	try{  

		InputStreamReader content = new InputStreamReader(rawContent, "UTF-8");

		//Create and parse JSON object - will contain fields name, alpha2_code and alpha3_code
		JSONParser jPar= new JSONParser();
		JSONObject jObj = (JSONObject)jPar.parse(content);

		System.out.println("JSON Object: " + jObj);

		JSONObject restResponse = (JSONObject)jObj.get("RestResponse");
		JSONObject result = (JSONObject)restResponse.get("result");


		if(result.containsKey("name"))
		{
			String name = (String)result.get("name");
			System.out.println("Name is "+name);

			String alpcode2 = (String)result.get("alpha2_code");
			System.out.println("Alpha Code 2 is "+alpcode2);

			String alpcode3 = (String)result.get("alpha3_code");
			System.out.println("Alpha Code 3 is "+alpcode3);
		}

	}catch (Exception e) {
		e.printStackTrace();
	}      
}
```

Then go to your `main` method, comment out the call to `printContent`, and add a call to `printContentWithJSON`.

```java
public static void main(String[] args){

        URL serviceRequest = getServiceRequest();
        InputStream rawContent = getContent(serviceRequest);

        //printContent(rawContent);
        printContentWithJSON(rawContent);

    }
```

### Screenshot of output:



