            

# Week 3 Lab—Data Types and Assignment Statements

## Scenario

In this week’s lab, you will look at data types and memory leaks and look at a variety of IDEs.

## Rubric

Point distribution for this activity:


| **Document**     | **Points possible** |
|:---------------- |:-------------------:|
| Part A           |         20          |
| Part B           |         30          |
| **Total Points** |       **50**        |


## PART A:

One of the dangers with C++ pointers is memory leaks. Run the following code in C++. If you are using g++, then run the compiler with the `-fsanitize=leak` option.

```c++
/* Visual C++ only
#define _CRTDBG_MAP_ALLOC
#define _CRT_SECURE_NO_WARNINGS
#include <crtdbg.h> 
*/
#include <stdlib.h>
#include <string>
#include <cstring>

void memLeak();

int main(int argc, char* argv[])

{

	memLeak();
	_CrtDumpMemoryLeaks(); // Visual C++ only
	return 0;

}

void memLeak()
{

	int* p = new int;
	char* string1 = new char[20];
	char* string2 = new char[25];

	strcpy(string1, "Sheldon");

	string2 = string1;

	delete p;

}
```

When you run this code you should see the following showing the memory leaks:

```
Detected memory leaks!
Dumping objects ->
{72} normal block at 0x02BA6250, 25 bytes long.
 Data: < > CD CD CD CD CD CD CD CD CD CD CD CD CD CD CD CD
{71} normal block at 0x02BA97F8, 20 bytes long.
 Data: <Sheldon> 53 68 65 6C 64 6F 6E 00 CD CD CD CD CD CD CD CD
```

or if you are using g++:

```
=================================================================
==6453==ERROR: LeakSanitizer: detected memory leaks

Direct leak of 25 byte(s) in 1 object(s) allocated from:
    #0 0x7faccf190511 in operator new[](unsigned long) (/lib/x86_64-linux-gnu/liblsan.so.0+0x10511)
    #1 0x7faccfab61d9 in memLeak() ${filePath}:21      
    #2 0x7faccfab61a0 in main ${filePath}:11
    #3 0x7faccefa70b2 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x270b2)

Direct leak of 20 byte(s) in 1 object(s) allocated from:
    #0 0x7faccf190511 in operator new[](unsigned long) (/lib/x86_64-linux-gnu/liblsan.so.0+0x10511)
    #1 0x7faccfab61cb in memLeak() ${filePath}:20      
    #2 0x7faccfab61a0 in main ${filePath}:11
    #3 0x7faccefa70b2 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x270b2)

SUMMARY: LeakSanitizer: 45 byte(s) leaked in 2 allocation(s).
```

How would you fix it so that the value in string1 and string2 are both “Sheldon” but with no memory leaks?

C++ code:

Screenshot of output:
	
## Part B: 

In our labs, we have always used an IDE. Now let’s compare code by creating the same code using a command line. Either download Cygwin on your machine, use your Raspberry Pi, any other Linux machine, or open Cygwin from [http://lab.devry.edu](http://lab.devry.edu)

Type `vi hello.cpp`

In vi, you need to type i to get in insert mode and escape to go to command mode. Type i then the following code.

```c++
#include <iostream>

using namespace std;

int main(void)
{
	cout << "Hello, World!" << endl;
	cout << "Press any key to exit" << flush;
	cin.ignore( );

	return 0;
}
```

Once you’ve typed in the code, press the escape key, then type in `:wq!` This will save and quit.

Now type in `g++ hello.cpp -o hello`

Then `./hello`

This will run your code.

Next run Hello World in C#, Java (download Eclipse or use it in citrix: lab.devry.edu or try Netbeans), and Python (IDLE). Python is available on citrix: lab.devry.edu or on the Raspberry Pi or by downloading Python [https://www.python.org/downloads/](https://www.python.org/downloads/) . Feel free to try another language (ruby/kotlin/C/perl).

Paste screenshots of all code here.

1. C++ command line in linux

2. C#

3. Java

4. Python


Compare: g++ command line, Visual Studio, Netbeans/Eclipse, and Idle. Which do you like the best? Why?