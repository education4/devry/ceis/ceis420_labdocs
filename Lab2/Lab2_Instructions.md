            

# Week 2 Lab—Syntax and Runtime Management of Dynamic Structures

## Scenario

In this week’s lab, you will look at timing operations and how different operations can take a different amount of time to complete.

## Rubric

Point distribution for this activity:

| **Document**       | **Points possible** |
|:------------------ |:-------------------:|
| Part A             |         30          |
| Part B             |         20          |
| _**Total Points**_ |         50          |

## PART A

### C++

We will look at timing operations in C++. To see the difference in operations, write C++ code to compare `cout` and `printf` and display the time difference for 100 `cout` operations and 100 `printf` operations. This code can be written in Visual Studio. Below is how you would time the 100 `cout` operations in Visual Studio. Add another for loop and display time for both `cout` and `printf` then comment about why you think there is a difference.

```c++
##include <iostream>
##include <time.h>

using namespace std;

int main()
{
	double start, stop;
	start=clock();

	for (int i=0;i<100;i++)
	{
		cout << " The number is "+i << endl;
	}

	stop =clock();

	cout << "It took " << (double(stop-start)/CLOCKS_PER_SEC) << " seconds for cout"<<endl;
	cin.ignore();
}
```

C++ code:

Screenshot of output:

Why is one faster than the other?

### Java

Try this code in Java. Use Eclipse or Netbeans to see the difference in operations. Add for loops and display time for both `System.out.pritnln` and `System.out.printf`, then comment about why you think there is a difference.

```java
public class optMain {

	public static void main(String\[\] args) {
		long start, stop\=0;
		start = System._nanoTime_();

		for (int i\=0;i<100;i++)
		{
			System.out.println(" The number is "+i);
		}

		stop = System._nanoTime_()-start;

		System.out.println("It took "+stop+" nano seconds for code to run");
	}
}
```

Java code:

Screenshot of output:

Why is one faster than the other?

## Part B

### Prompt

Next, we will take timing a step further. Feel free to use C++, C#, or Java `ArrayList` for this next exercise. Determine the runtime differences between a fixed array and a dynamic array. Add several items to each array (I added 100000). A fixed array is created in the stack and allocated at compile time whereas a dynamic array is created in the heap and allocated at run time.

Copy your C++ or Java Code here:

Fixed array time (this may be 0):  

Dynamic array time: 

**Explain the difference between the stack and the heap below.**

### Example output

![](ExampleOutput_ArrayListRuntime.png)