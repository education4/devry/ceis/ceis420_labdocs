# Week 4 Lab—`Goto`s and Regular Expressions

## Scenario 

In this week’s lab, you will look at Designing code and regular expressions.  

## Rubric

| **Document**     | **Points possible** | 
|:---------------- |:-------------------:|
| Part A           |         20          |
| Part B           |         30          |
| **Total Points** |       **50**        |

## Part A:

In a letter to the editor of CACM, Rubin (1987) uses the following code segment as evidence that the readability of some code with gotos is better than the equivalent code without gotos. This code finds the first row of an n-by-n integer matrix named x that has nothing but 0 values.

```c
for (i = 1; i <= n; i++) {
  for (j = 1; j <= n; j++)
     if (x[i][j] != 0)
      goto reject;
  println ('First -all--zero row is:', i);
  break;
reject:
 }
```
 
Rewrite this code without gotos in any language of your choice. Compare the readability of your code to that of the example code (should be at least one paragraph). 

## Part B:

Research online about pattern matching and regular expressions. 

1.	What is a regular expression? 
2.	Locate an example online and explain what it does: _________________
3.	We are going to use the following regular expression to search for a phone number. 

	1.	What does the \d indicate? 
	2.	What does the {3} indicate?
		
```regex
\(?\d{3}\)?[. -]? *\d{3}[. -]? *[. -]?\d{4}
```

6.	Feel free to create a form/GUI or use console to create a regular expression to determine if a phone number input is valid. You will need to import a regular expression library. My code below as an example.

```java
         string phoneNum =txtName.Text;
         string MatchPhonePattern = @"\(?\d{3}\)?[. -]? *\d{3}[. -]? *[. -]?\d{4}";
      Regex rx = new Regex(MatchPhonePattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        Match match = rx.Match(phoneNum);
        if (match.Success)
            lblDisplay.Text = "Valid number";
        else
            lblDisplay.Text = "not valid";
```       

Run your program and include a screenshot of your code running, and copy and paste your code. Use phone number as a regular expression, then validate a social security number using regular expressions. 

![valid entry example](validEntry.png)

![invalid entry example](invalidEntry.png)

Code for Phone number regex:

Screenshot:

Code for Social Security number regex:

Screenshot:
